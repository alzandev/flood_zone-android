**Flood Zone - Sistema de Alerta de Alagamentos e Elevação do Nível dos Rios**


Esse aplicativo tem como objetivo sinalizar os pontos no mapa e disparar alertas para os usuários, a fim de alertar a população e os órgãos públicos responsáveis sobre os pontos de enchentes e alagamentos no estado de São Paulo.


Aplicativo desenvolvido como TCC de Ciência da Computação