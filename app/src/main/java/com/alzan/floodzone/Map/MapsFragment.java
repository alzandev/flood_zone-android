package com.alzan.floodzone.Map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alzan.floodzone.R;
import com.alzan.floodzone.RecyclerItemClickListener;
import com.alzan.floodzone.Map.Status.RecyclerAdapter;
import com.alzan.floodzone.Map.Status.StatusItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Calazans on 09/03/2018.
 */

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap googleMap;
    MapView mapView;
    public Activity activity;
    Context context;
    Bundle savedInstanceState;
    static String local;
    static int status;

    public GoogleMap getGoogleMap() {
        return googleMap;
    }

    private LinearLayout mBottomSheet;

    private ImageView mLeftArrow;
    private ImageView mRightArrow;

    public TextView statusText;
    public CardView cardView;

    FloatingActionButton fab;

    static ArrayList<Integer> valueArraylist = new ArrayList<Integer>();
    static ArrayList<String> keyArraylist = new ArrayList<String>();

    public static ArrayList<Integer> getValueArraylist() {
        return valueArraylist;
    }

    public static void setValueArraylist(ArrayList<Integer> valueArraylist) {
        MapsFragment.valueArraylist = valueArraylist;
    }

    public static ArrayList<String> getKeyArraylist() {
        return keyArraylist;
    }

    public static void setKeyArraylist(ArrayList<String> keyArraylist) {
        MapsFragment.keyArraylist = keyArraylist;
    }

    static Map hashMap = new HashMap();

    public Map getHashMap() {
        return hashMap;
    }

    public void setHashMap(Map hashMap) {
        MapsFragment.hashMap = hashMap;
        //        Log.d("HASH 01/10", hashMap.toString());
    }

    public int getStatus() {
        return status;
    }

    public static void setStatus(int status) {
        MapsFragment.status = status;
        //        Log.e("JSON STATUS", String.valueOf(status));
        //        Log.e("JSON THIS STATUS", String.valueOf(MapsFragment.status));
        //        localArray.add(String.valueOf(local));
    }

    public String getLocal() {
        return local;
    }

    public static void setLocal(String local) {
        MapsFragment.local = local;
        Log.e("JSON LOCAL", local);
    }

    LinearLayout bottom_layout;

    static ArrayList<Double> latI;
    static ArrayList<Double> longI;

    static ArrayList<Double> lat2;
    static ArrayList<Double> long2;

    static ArrayList<Double> lat3;
    static ArrayList<Double> long3;

    static ArrayList<Double> lat4;
    static ArrayList<Double> long4;

    static ArrayList<Double> lat5;
    static ArrayList<Double> long5;

    static ArrayList<Double> lat6;
    static ArrayList<Double> long6;

    static ArrayList<Double> lat7;
    static ArrayList<Double> long7;

    static ArrayList<Double> lat8;
    static ArrayList<Double> long8;

    static ArrayList<Double> latF;
    static ArrayList<Double> longF;

    static ArrayList<Double> latR;
    static ArrayList<Double> longR;

    ArrayList<ArrayList<Double>> routeArray = new ArrayList<ArrayList<Double>>();


    static ArrayList<Integer> statusLevel;
    static ArrayList<String> localArray;
    LatLng center;
    static LatLng newCenter;

    public static void setLat2(ArrayList<Double> lat2) {
        MapsFragment.lat2 = lat2;
    }

    public static void setLong2(ArrayList<Double> long2) {
        MapsFragment.long2 = long2;
    }

    public static void setLat3(ArrayList<Double> lat3) {
        MapsFragment.lat3 = lat3;
    }

    public static void setLong3(ArrayList<Double> long3) {
        MapsFragment.long3 = long3;
    }

    public static void setLat4(ArrayList<Double> lat4) {
        MapsFragment.lat4 = lat4;
    }

    public static void setLong4(ArrayList<Double> long4) {
        MapsFragment.long4 = long4;
    }

    public static void setLat5(ArrayList<Double> lat5) {
        MapsFragment.lat5 = lat5;
    }

    public static void setLong5(ArrayList<Double> long5) {
        MapsFragment.long5 = long5;
    }

    public static void setLat6(ArrayList<Double> lat6) {
        MapsFragment.lat6 = lat6;
    }

    public static void setLong6(ArrayList<Double> long6) {
        MapsFragment.long6 = long6;
    }

    public static void setLat7(ArrayList<Double> lat7) {
        MapsFragment.lat7 = lat7;
    }

    public static void setLong7(ArrayList<Double> long7) {
        MapsFragment.long7 = long7;
    }

    public static void setLat8(ArrayList<Double> lat8) {
        MapsFragment.lat8 = lat8;
    }

    public static void setLong8(ArrayList<Double> long8) {
        MapsFragment.long8 = long8;
    }

    public static void setLocalArray(ArrayList<String> localArray) {
        MapsFragment.localArray = localArray;
    }

    public static void setLatI(ArrayList<Double> latI) {
        MapsFragment.latI = latI;
    }

    public static void setLongI(ArrayList<Double> longI) {
        MapsFragment.longI = longI;
    }

    public static void setLatF(ArrayList<Double> latF) {
        MapsFragment.latF = latF;
    }

    public static void setLongF(ArrayList<Double> longF) {
        MapsFragment.longF = longF;
    }

    public static void setStatusLevel(ArrayList<Integer> statusLevel) {
        MapsFragment.statusLevel = statusLevel;
    }

    public static void setLatR(ArrayList<Double> latR) {
        MapsFragment.latR = latR;
    }

    public static void setLongR(ArrayList<Double> longR) {
        MapsFragment.longR = longR;
    }

    public static LatLng onClickInLocal(final LatLng latLng) {
        newCenter = latLng;
        System.out.println(" ====> " + latLng);
        return latLng;
    }

    public static final String TAG = "com.alzan.floodzone.Map.MapsFragment";

    public static MapsFragment newInstance() {
        MapsFragment fragment = new MapsFragment();
        return fragment;
    }

    static int updateMapView;

    public static int getUpdateMapView() {
        return updateMapView;
    }

    public static void setUpdateMapView(int updateMapView) {
        MapsFragment.updateMapView = updateMapView;

    }

    View view;
    public PopupWindow popupWindow;

    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;

    @SuppressLint("MissingPermission")
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_maps, container, false);

        statusText = (TextView) view.findViewById(R.id.statusText);
        cardView = (CardView) view.findViewById(R.id.cardView);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.onResume(); // needed to get the map to display immediately

        getActivity().setTitle("Mapa");

        mapHelper(center, view);
        mapView(false);

        final Bundle b = new Bundle();
        ArrayList<StatusItem> list = new ArrayList<StatusItem>();
        System.out.println("list   =>  " + list);


        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = (LayoutInflater)
                        getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = inflater.inflate(R.layout.status_popup, null);

                int width = LinearLayout.LayoutParams.MATCH_PARENT;
                int height = LinearLayout.LayoutParams.MATCH_PARENT;
                boolean focusable = true;
                popupWindow = new PopupWindow(popupView, width, height, focusable);

                popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
                recyclerView = (RecyclerView) popupView.findViewById(R.id.recyclerView);
                final ProgressBar progressBar = (ProgressBar) popupView.findViewById(R.id.progressBar);

                new JSONTaskPopUp(getContext(), recyclerView, recyclerAdapter, progressBar).execute();
//
//                Thread thread = new Thread() {
//                    @Override
//                    public void run() {
//                        try {
//                            while (!isInterrupted()) {
//                                Thread.sleep(1000);
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        new JSONTaskPopUp(getContext(), recyclerView, recyclerAdapter, progressBar).execute();
//
//                                    }
//                                });
//                            }
//                        } catch (Exception ex) {
//                        }
//                    }
//                };
//
//                thread.start();

                RelativeLayout relativeLayout = (RelativeLayout) popupView.findViewById(R.id.dismiss);
                relativeLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popupWindow.dismiss();
                    }
                });

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);

                recyclerView.addOnItemTouchListener(
                        new RecyclerItemClickListener(context, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {


                                final double lat = latR.get(position);
                                final double lng = longR.get(position);

                                System.out.println("CLICK LATLNG => " + lat + lng);

                                mapView.getMapAsync(new OnMapReadyCallback() {
                                    @Override
                                    public void onMapReady(GoogleMap mMap2) {

                                        center = new LatLng(lat, lng);

                                        googleMap = mMap2;
                                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(center));
                                        googleMap.setMaxZoomPreference(17.5f);
                                        googleMap.setMinZoomPreference(13.0f);
                                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(16.0f));
                                        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style_maps));
                                    }
                                });
                                popupWindow.dismiss();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                // do whatever
                            }
                        })
                );


            }
        });


        System.out.println("    ONCREATEVIEW   ");


        return view;
    }

    public void openPopUp() {

    }


    public void mapHelper(LatLng latLng, View view) {

        if (latLng != null) {
            center = latLng;
            System.out.println("LatLng => " + latLng.toString());
        } else {
            try {
                LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                Location location = locationManager.getLastKnownLocation(locationManager.PASSIVE_PROVIDER);

                try {
                    center = new LatLng(location.getLatitude(), location.getLongitude());
                } catch (Exception ex) {
                    center = new LatLng(-23.541498, -46.739249);
//                    showLocationMessage();
                    Toast.makeText(getContext(), "Sua localização está desativada!\nPor favor, ative sua localização", Toast.LENGTH_LONG).show();
                }
            } catch (Exception ex) {
//                showLocationMessage();
//                Toast.makeText(getContext(), "Sua localização está desativada!\nPor favor, ative sua localização", Toast.LENGTH_LONG).show();
            }
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap2) {
//                    LatLng center = new LatLng(finalLat, finalLong);
                googleMap = mMap2;
//                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(center));
                googleMap.setMaxZoomPreference(17.5f);
                googleMap.setMinZoomPreference(13.0f);
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(15.9f));
                googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.style_maps));
            }
        });

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showLocationMessage() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle("Ative sua localização");
        builder.setMessage("Sua localização está desativada! Deseja ativar?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                builder.create().dismiss();
            }
        });

        builder.show();
    }

    public void mapView(boolean moveCamera) {
        new JSONTask(view).execute();
        Log.d("LATLONG", latI + " " + longF);


        //        Log.d("HASH TO POLYLINE VALUE", String.valueOf(valueArraylist.size()));
        //        Log.d("HASH TO POLYLINE KEY", keyArraylist.toString());


        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new JSONTask(view).execute();
                                Log.d("LATLONG", statusLevel + " " + latI + " " + longF + " " + statusLevel);

                            }
                        });
                    }
                } catch (Exception ex) {
                }
            }
        };

        thread.start();


        Thread thread2 = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(5000);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                //                                Log.d("HASH TO POLYLINE 3", String.valueOf(hashMap.size()));

                                String key = null;
                                int value = 0;

                                int max = 0;
                                int qnt = 0;

                                for (int j = 0; j < hashMap.size(); j++) {
                                    value = new ArrayList<Integer>(hashMap.values()).get(j);
                                    if (max > value) {
                                    } else {
                                        max = value;
                                    }
                                }


                                for (int i = 0; i < hashMap.size(); i++) {
                                    //                                    String value = (new ArrayList<String>(hashMap.values())).get(i);
                                    key = new ArrayList<String>(hashMap.keySet()).get(i);
                                    value = new ArrayList<Integer>(hashMap.values()).get(i);

                                    if (max == value) {
                                        qnt++;
                                    }

                                    //                                    Log.d("HASH 1965/8", String.valueOf(key));
                                    //                                    Log.d("HASH 1965/7", String.valueOf(value));
                                    //                                    Log.d("LARGEST 1965/10", String.valueOf(qnt));
                                    cardView.setVisibility(View.VISIBLE);

                                    if (max == 1 && value == 1) {
                                        statusText.setText(R.string.normal);
                                        cardView.setCardBackgroundColor(Color.parseColor("#f700af6c"));
                                    }

                                    if (max == 2 && value == 2 && qnt > 1) {
                                        statusText.setText("Varios locais em estado de atenção!");
                                        cardView.setCardBackgroundColor(Color.parseColor("#F7FFEA00"));
                                    } else if (max == 2 && value == 2) {
                                        statusText.setText(key + " em estado de atenção!");
                                        cardView.setCardBackgroundColor(Color.parseColor("#F7FFEA00"));
                                    }

                                    if (max == 3 && value == 3 && qnt > 1) {
                                        statusText.setText("Varios locais em estado de alerta!");
                                        cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
                                    } else if (max == 3 && value == 3) {
                                        statusText.setText(key + " em estado de alerta!");
                                        cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
                                    }
                                }

                                mapView.getMapAsync(new OnMapReadyCallback() {
                                    @SuppressLint("MissingPermission")
                                    @Override
                                    public void onMapReady(GoogleMap mMap2) {
                                        googleMap = mMap2;

                                        String key = null;
                                        int value = 0;
                                        Polyline polyline1 = null;
                                        googleMap.clear();

                                        googleMap.setMyLocationEnabled(true);


                                        LocationManager locationManager = (LocationManager) getContext().getSystemService(LOCATION_SERVICE);
                                        Location location = locationManager.getLastKnownLocation(locationManager.PASSIVE_PROVIDER);


                                        for (int i = 0; i < hashMap.size(); i++) {
                                            key = new ArrayList<String>(hashMap.keySet()).get(i);
                                            value = new ArrayList<Integer>(hashMap.values()).get(i);


                                            if (statusLevel.get(i) == 1) {
                                            } else if (statusLevel.get(i) == 2) {
                                                googleMap.addPolyline(new PolylineOptions()
                                                        .add(new LatLng(latI.get(i), longI.get(i)),
                                                                new LatLng(lat2.get(i), long2.get(i)),
                                                                new LatLng(lat3.get(i), long3.get(i)),
                                                                new LatLng(lat4.get(i), long4.get(i)),
                                                                new LatLng(lat5.get(i), long5.get(i)),
                                                                new LatLng(lat6.get(i), long6.get(i)),
                                                                new LatLng(lat7.get(i), long7.get(i)),
                                                                new LatLng(lat8.get(i), long8.get(i)),
                                                                new LatLng(latF.get(i), longF.get(i)))
                                                        .clickable(true)
                                                        .width(10)
                                                        .color(Color.rgb(255, 251, 0))).setTag(localArray.get(i) + " X " + 2);


                                                googleMap.addCircle(new CircleOptions()
                                                        .center(new LatLng(latI.get(i), longI.get(i)))
                                                        .radius(1)
                                                        .clickable(true)
                                                        .strokeColor(Color.rgb(255, 251, 0))
                                                        .fillColor(Color.rgb(255, 251, 0)));

                                                googleMap.addCircle(new CircleOptions()
                                                        .center(new LatLng(latF.get(i), longF.get(i)))
                                                        .radius(1)
                                                        .clickable(true)
                                                        .strokeColor(Color.rgb(255, 251, 0))
                                                        .fillColor(Color.rgb(255, 251, 0)));

                                            } else if (statusLevel.get(i) == 3) {

                                                googleMap.addCircle(new CircleOptions()
                                                        .center(new LatLng(latI.get(i), longI.get(i)))
                                                        .radius(1)
                                                        .clickable(true)
                                                        .strokeColor(Color.rgb(255, 0, 0))
                                                        .fillColor(Color.rgb(255, 0, 0)));

                                                googleMap.addCircle(new CircleOptions()
                                                        .center(new LatLng(latF.get(i), longF.get(i)))
                                                        .radius(1)
                                                        .clickable(true)
                                                        .strokeColor(Color.rgb(255, 0, 0))
                                                        .fillColor(Color.rgb(255, 0, 0)));

                                                googleMap.addPolyline(new PolylineOptions()
                                                        .add(new LatLng(latI.get(i), longI.get(i)),
                                                                new LatLng(lat2.get(i), long2.get(i)),
                                                                new LatLng(lat3.get(i), long3.get(i)),
                                                                new LatLng(lat4.get(i), long4.get(i)),
                                                                new LatLng(lat5.get(i), long5.get(i)),
                                                                new LatLng(lat6.get(i), long6.get(i)),
                                                                new LatLng(lat7.get(i), long7.get(i)),
                                                                new LatLng(lat8.get(i), long8.get(i)),
                                                                new LatLng(latF.get(i), longF.get(i)))
                                                        .width(10)
                                                        .clickable(true)
                                                        .color(Color.rgb(255, 0, 0))).setTag(localArray.get(i) + " X " + 3);
                                            }
                                        }
                                    }
                                });
                            }
                        });
                    }
                } catch (Exception ex) {
                }
            }
        };

        // ITEM 1[[LAT 1, LAT 1], [LONG 1, LONG1]], ITEM 2[[LAT 1, LAT 1], [LONG 1, LONG 1]]

        thread2.start();


        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap2) {
//                    LatLng center = new LatLng(finalLat, finalLong);
                googleMap = mMap2;

                googleMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener() {
                    @Override
                    public void onPolylineClick(Polyline polyline) {
                        try {

                            String[] parts = polyline.getTag().toString().split(" X ");
                            String local = parts[0];
                            String status = parts[1];

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            LayoutInflater inflater = getActivity().getLayoutInflater();
                            builder.setView(inflater.inflate(R.layout.alert_dialog, null));

                            final AlertDialog dialog = builder.create();
                            dialog.show();

                            TextView title = (TextView) dialog.findViewById(R.id.title);
                            TextView text = (TextView) dialog.findViewById(R.id.text);
                            Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
                            ImageView imageView = (ImageView) dialog.findViewById(R.id.image);


                            if (status.equals("1")) {
                            }
                            if (status.equals("2")) {
                                imageView.setImageResource(R.drawable.ic_warning_circle_yellow);
                                title.setTextColor(Color.rgb(234, 209, 47));
                                title.setText(local);
                                btnOk.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.alert)));
                                text.setText("Em estado de atenção!\n" + "Local apresenta riscos de alagamento");
                            } else if (status.equals("3")) {
                                imageView.setImageResource(R.drawable.ic_warning_circle);
                                title.setText(local);
                                title.setTextColor(Color.rgb(221, 44, 0));
                                text.setText("Em estado de alerta!\n" + "Local alagado, região possivelmente intransitável!");
                            }

                            btnOk.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });


                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        System.out.println("STATUS => RESUME");
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        System.out.println("STATUS => PAUSED");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        System.out.print("GOOGLE MAP => ");
    }


    @Override
    public void onMapClick(LatLng latLng) {

    }
}

class JSONTaskPopUp extends AsyncTask<String, Integer, Integer> {

    String URL = "http://alzan.com.br/FloodZone/flood.php";

    public Context activity;
    ArrayList<StatusItem> list = new ArrayList<StatusItem>();
    public RecyclerView recyclerView;
    public RecyclerAdapter recyclerAdapter;
    public ProgressBar progressBar;

    public JSONTaskPopUp(Context activity, RecyclerView recyclerView, RecyclerAdapter recyclerAdapter, ProgressBar progressBar) {
        this.activity = activity;
        this.recyclerView = recyclerView;
        this.recyclerAdapter = recyclerAdapter;
        this.progressBar = progressBar;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(URL);
            HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            String json = reader.readLine();

            JSONObject response = new JSONObject(json);
            JSONArray jsonArray = response.getJSONArray("flood");
            list.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                StatusItem statusItem = new StatusItem();
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                int id = jsonObject.isNull("ID") ? null : jsonObject.getInt("ID");
                statusItem.setId(id);

                int status = jsonObject.isNull("STATUS") ? null : jsonObject.getInt("STATUS");
                statusItem.setStatus(status);

                String timeStamp = jsonObject.isNull("TIMESTAMP") ? null : jsonObject.getString("TIMESTAMP");
                statusItem.setTimeStamp(timeStamp);

                String local = jsonObject.isNull("LOCAL") ? null : jsonObject.getString("LOCAL");
                statusItem.setLocal(local);


                int type = jsonObject.isNull("TYPE") ? null : jsonObject.getInt("TYPE");
                statusItem.setType(type);

                double latI = jsonObject.isNull("LATITUDE_I") ? null : jsonObject.getDouble("LATITUDE_I");
                statusItem.setLatI(latI);

                double longI = jsonObject.isNull("LONGITUDE_I") ? null : jsonObject.getDouble("LONGITUDE_I");
                statusItem.setLongI(longI);

                double latF = jsonObject.isNull("LATITUDE_F") ? null : jsonObject.getDouble("LATITUDE_F");
                statusItem.setLatF(latF);

                double longF = jsonObject.isNull("LONGITUDE_F") ? null : jsonObject.getDouble("LONGITUDE_F");
                statusItem.setLongF(longF);

//                float[] distance = new float[1];
//                Location.distanceBetween(latI, longI, latF, longF, distance);
//
//                Location location = new Location("MyLocal");
//                location.setLatitude(latI);
//                location.setLongitude(longI);
//
//                Location newlocation = new Location("NewLocal");
//                location.setLatitude(latF);
//                location.setLongitude(longF);
//

                list.add(statusItem);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(Integer numero) {
        recyclerAdapter = new RecyclerAdapter(activity, R.layout.status_list, list);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }
}


class JSONTask extends AsyncTask<String, Integer, Integer> {

    String URL = "http://alzan.com.br/FloodZone/flood.php";
    private View rootView;

    public JSONTask() {

    }

    public JSONTask(View rootView) {
        this.rootView = rootView;
    }

    public static final String TAG = MapsFragment.class.getSimpleName();


    @Override
    protected void onPreExecute() {
        //Codigo
    }

    @Override
    protected Integer doInBackground(String... params) {
        try {

            new MapsFragment().statusText = (TextView) rootView.findViewById(R.id.statusText);
            new MapsFragment().cardView = (CardView) rootView.findViewById(R.id.cardView);

            DefaultHttpClient defaultClient = new DefaultHttpClient();
            HttpGet httpGetRequest = new HttpGet(URL);

            HttpResponse httpResponse = defaultClient.execute(httpGetRequest);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            String json = reader.readLine();

            JSONObject response = new JSONObject(json);

            JSONArray jsonArray = response.getJSONArray("flood");

            Map hashMap = new HashMap();
            ArrayList<String> key = new ArrayList<String>();
            ArrayList<Integer> value = new ArrayList<Integer>();

            ArrayList<Double> latI = new ArrayList<Double>();
            ArrayList<Double> longI = new ArrayList<Double>();

            ArrayList<Double> lat2 = new ArrayList<Double>();
            ArrayList<Double> long2 = new ArrayList<Double>();

            ArrayList<Double> lat3 = new ArrayList<Double>();
            ArrayList<Double> long3 = new ArrayList<Double>();

            ArrayList<Double> lat4 = new ArrayList<Double>();
            ArrayList<Double> long4 = new ArrayList<Double>();

            ArrayList<Double> lat5 = new ArrayList<Double>();
            ArrayList<Double> long5 = new ArrayList<Double>();

            ArrayList<Double> lat6 = new ArrayList<Double>();
            ArrayList<Double> long6 = new ArrayList<Double>();

            ArrayList<Double> lat7 = new ArrayList<Double>();
            ArrayList<Double> long7 = new ArrayList<Double>();

            ArrayList<Double> lat8 = new ArrayList<Double>();
            ArrayList<Double> long8 = new ArrayList<Double>();

            ArrayList<Double> latF = new ArrayList<Double>();
            ArrayList<Double> longF = new ArrayList<Double>();

            ArrayList<Double> latR = new ArrayList<Double>();
            ArrayList<Double> longR = new ArrayList<Double>();

            ArrayList<ArrayList<Double>> routeArrayLat = new ArrayList<ArrayList<Double>>();
            ArrayList<ArrayList<Double>> routeArrayLng = new ArrayList<ArrayList<Double>>();

            ArrayList<String> localArray = new ArrayList<>();

            ArrayList<Integer> status = new ArrayList<Integer>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                int id = jsonObject.isNull("ID") ? null : jsonObject.getInt("ID");
                int flood = jsonObject.isNull("STATUS") ? null : jsonObject.getInt("STATUS");
                String datehour = jsonObject.isNull("DATEHOUR") ? null : jsonObject.getString("DATEHOUR");
                String local = jsonObject.isNull("LOCAL") ? null : jsonObject.getString("LOCAL");

                double latIDouble = jsonObject.isNull("LATITUDE_I") ? null : jsonObject.getDouble("LATITUDE_I");
                double longIDouble = jsonObject.isNull("LONGITUDE_I") ? null : jsonObject.getDouble("LONGITUDE_I");

                double lat2Double = jsonObject.isNull("LATITUDE_2") ? null : jsonObject.getDouble("LATITUDE_2");
                double long2Double = jsonObject.isNull("LONGITUDE_2") ? null : jsonObject.getDouble("LONGITUDE_2");

                double lat3Double = jsonObject.isNull("LATITUDE_3") ? null : jsonObject.getDouble("LATITUDE_3");
                double long3Double = jsonObject.isNull("LONGITUDE_3") ? null : jsonObject.getDouble("LONGITUDE_3");

                double lat4Double = jsonObject.isNull("LATITUDE_4") ? null : jsonObject.getDouble("LATITUDE_4");
                double long4Double = jsonObject.isNull("LONGITUDE_4") ? null : jsonObject.getDouble("LONGITUDE_4");

                double lat5Double = jsonObject.isNull("LATITUDE_5") ? null : jsonObject.getDouble("LATITUDE_5");
                double long5Double = jsonObject.isNull("LONGITUDE_5") ? null : jsonObject.getDouble("LONGITUDE_5");

                double lat6Double = jsonObject.isNull("LATITUDE_6") ? null : jsonObject.getDouble("LATITUDE_6");
                double long6Double = jsonObject.isNull("LONGITUDE_6") ? null : jsonObject.getDouble("LONGITUDE_6");

                double lat7Double = jsonObject.isNull("LATITUDE_7") ? null : jsonObject.getDouble("LATITUDE_7");
                double long7Double = jsonObject.isNull("LONGITUDE_7") ? null : jsonObject.getDouble("LONGITUDE_7");

                double lat8Double = jsonObject.isNull("LATITUDE_8") ? null : jsonObject.getDouble("LATITUDE_8");
                double long8Double = jsonObject.isNull("LONGITUDE_8") ? null : jsonObject.getDouble("LONGITUDE_8");

                double latFDouble = jsonObject.isNull("LATITUDE_F") ? null : jsonObject.getDouble("LATITUDE_F");
                double longFDouble = jsonObject.isNull("LONGITUDE_F") ? null : jsonObject.getDouble("LONGITUDE_F");

                double latRDouble = jsonObject.isNull("LATITUDE_R") ? null : jsonObject.getDouble("LATITUDE_R");
                double longRDouble = jsonObject.isNull("LONGITUDE_R") ? null : jsonObject.getDouble("LONGITUDE_R");

                //https://admin.mysql.uhserver.com/index.php

                latI.add(latIDouble);
                longI.add(longIDouble);

                lat2.add(lat2Double);
                long2.add(long2Double);

                lat3.add(lat3Double);
                long3.add(long3Double);

                lat4.add(lat4Double);
                long4.add(long4Double);

                lat5.add(lat5Double);
                long5.add(long5Double);

                lat6.add(lat6Double);
                long6.add(long6Double);

                lat7.add(lat7Double);
                long7.add(long7Double);

                lat8.add(lat8Double);
                long8.add(long8Double);

                latF.add(latFDouble);
                longF.add(longFDouble);

                latR.add(latRDouble);
                longR.add(longRDouble);

                status.add(flood);

                hashMap.put(local, flood);

                key.add(i, local);
                value.add(i, flood);
                localArray.add(local);
            }

            new MapsFragment().setValueArraylist(value);
            new MapsFragment().setKeyArraylist(key);
            new MapsFragment().setHashMap(hashMap);

            new MapsFragment().setLatI(latI);
            new MapsFragment().setLongI(longI);

            new MapsFragment().setLat2(lat2);
            new MapsFragment().setLong2(long2);

            new MapsFragment().setLat3(lat3);
            new MapsFragment().setLong3(long3);

            new MapsFragment().setLat4(lat4);
            new MapsFragment().setLong4(long4);

            new MapsFragment().setLat5(lat5);
            new MapsFragment().setLong5(long5);

            new MapsFragment().setLat6(lat6);
            new MapsFragment().setLong6(long6);

            new MapsFragment().setLat7(lat7);
            new MapsFragment().setLong7(long7);

            new MapsFragment().setLat8(lat8);
            new MapsFragment().setLong8(long8);


            new MapsFragment().setLatF(latF);
            new MapsFragment().setLongF(longF);

            new MapsFragment().setLatR(latR);
            new MapsFragment().setLongR(longR);

            new MapsFragment().setStatusLevel(status);
            new MapsFragment().setLocalArray(localArray);

            ArrayList valueArrayList;
            ArrayList keyArrayList;

            //            Log.d("HASH MAPX", hashMap.toString());
            //            Log.d("HASH MAP0", String.valueOf(Integer.parseInt(hashMap.get("Av Paulista").toString())));
            //            Log.d("HASH MAP1", String.valueOf(hashMap.get("Rio Pinheiros")));

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Integer numero) {
    }

    protected void onProgressUpdate(Integer params) {
        //Codigo
    }
}