package com.alzan.floodzone.Map.Status;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alzan.floodzone.Map.MainActivity;
import com.alzan.floodzone.Map.MapsFragment;
import com.alzan.floodzone.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Calazans on 15/07/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerHolder> {
    Context context;
    public Activity activity;
    ArrayList<StatusItem> items;
    private ArrayList<StatusItem> objectList;
    StatusItem object;
    MapsFragment mapsFragment;
    MainActivity mainActivity;

    public RecyclerAdapter(Context context, int textViewResourceId, ArrayList objects) {
        this.context = context;
        objectList = objects;
    }

    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_list, null);
        RecyclerHolder holder = new RecyclerHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerHolder holder, int position) {
        object = objectList.get(position);

//        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
//                Long.parseLong(object.getTimeStamp()),
//                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);


        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd hh:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(object.getTimeStamp());
            dateInMillis = date.getTime();
            Log.d("TIMESTAMP 96", String.valueOf(dateInMillis));

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    dateInMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            Log.d("TIMESTAMP 97", String.valueOf(timeAgo));

            holder.timeStamp.setText(timeAgo);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (object.getStatus() == 1) {
            holder.situation.setText("Normal!");
            holder.status.setText(object.getLocal() + " não apresenta nenhum ponto de alagamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f700af6c"));
        }

        if (object.getStatus() == 1 && object.getType() == 1) {
            holder.situation.setText("Normal!");
            holder.status.setText(object.getLocal() + " não apresenta nenhum ponto de transbordamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f700af6c"));
        }

        if (object.getStatus() == 2) {
            holder.situation.setText("Atenção!");
            holder.status.setText(object.getLocal() + " apresenta risco de alagamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7EAD12F"));
        }

        if (object.getStatus() == 2 && object.getType() == 1) {
            holder.situation.setText("Atenção!");
            holder.status.setText(object.getLocal() + " apresenta risco de transbordamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7EAD12F"));
        }

        if (object.getStatus() == 3) {
            holder.situation.setText("Alerta!");
            holder.status.setText(object.getLocal() + " está alagada, região possivelmente intransitável!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
        }

        if (object.getStatus() == 3 && object.getType() == 1) {
            holder.situation.setText("Alerta!");
            holder.status.setText(object.getLocal() + " transbordou, região possivelmente intransitável!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
        }
        double mLat = 0;
        double mLong = 0;

        mLat = ((object.getLatI() + object.getLatF()) / 2);
        mLong = (object.getLongI() + object.getLongF()) / 2;

        final double finalMLat = mLat;
        final double finalMLong = mLong;

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            System.out.println("CLICK ADAPTER");
            }
        });
    }

    public static long timeStamp(String srcDate) {
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd hh:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            Log.d("TIMESTAMP 96", String.valueOf(dateInMillis));

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    dateInMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            Log.d("TIMESTAMP 97", String.valueOf(timeAgo));


            return dateInMillis;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }
}
