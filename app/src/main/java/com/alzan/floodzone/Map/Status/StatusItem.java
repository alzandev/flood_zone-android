package com.alzan.floodzone.Map.Status;

/**
 * Created by Calazans on 15/07/2018.
 */

public class StatusItem {
    private int id;
    private int status;
    private String local;
    private String timeStamp;
    private double latI;
    private double longI;
    private double latF;
    private double longF;
    private int type;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public double getLatI() {
        return latI;
    }

    public void setLatI(double latI) {
        this.latI = latI;
    }

    public double getLongI() {
        return longI;
    }

    public void setLongI(double longI) {
        this.longI = longI;
    }

    public double getLatF() {
        return latF;
    }

    public void setLatF(double latF) {
        this.latF = latF;
    }

    public double getLongF() {
        return longF;
    }

    public void setLongF(double longF) {
        this.longF = longF;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public StatusItem() {
    }

    public StatusItem(int status, String local) {
        this.status = status;
        this.local = local;
    }

    public StatusItem(int id, int status, String local) {
        this.id = id;
        this.status = status;
        this.local = local;
    }

    public StatusItem(int id, int status, String local, String timeStamp) {
        this.id = id;
        this.status = status;
        this.local = local;
        this.timeStamp = timeStamp;
    }

}
