package com.alzan.floodzone.Statistic.Latest;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alzan.floodzone.Map.MainActivity;
import com.alzan.floodzone.Map.MapsFragment;
import com.alzan.floodzone.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Calazans on 12/09/2018.
 */

public class RecyclerAdapterAll extends RecyclerView.Adapter<RecyclerHolderAll> {

    Context context;
    public Activity activity;
    ArrayList<ListItems> items;
    private ArrayList<ListItems> objectList;
    ListItems object;
    MapsFragment mapsFragment;
    MainActivity mainActivity;

    public RecyclerAdapterAll(Context context, int textViewResourceId, ArrayList objects) {
        this.context = context;
        objectList = objects;
    }


    @Override
    public RecyclerHolderAll onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.status_list, null);
        RecyclerHolderAll holder = new RecyclerHolderAll(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerHolderAll holder, int position) {

        object = objectList.get(position);


        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd hh:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(object.getTimeStamp());
            dateInMillis = date.getTime();

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    dateInMillis,
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

            holder.timeStamp.setText(timeAgo);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (object.getStatus() == 1) {
            holder.situation.setText("Normal!");
            holder.status.setText(object.getLocal() + " não apresenta nenhum ponto de alagamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f700af6c"));
        }

        if (object.getStatus() == 1 && object.getType() == 1) {
            holder.situation.setText("Normal!");
            holder.status.setText(object.getLocal() + " não apresenta nenhum ponto de transbordamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#f700af6c"));
        }

        if (object.getStatus() == 2) {
            holder.situation.setText("Atenção!");
            holder.status.setText(object.getLocal() + " apresenta risco de alagamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7EAD12F"));
        }

        if (object.getStatus() == 2 && object.getType() == 1) {
            holder.situation.setText("Atenção!");
            holder.status.setText(object.getLocal() + " apresenta risco de transbordamento!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7EAD12F"));
        }

        if (object.getStatus() == 3) {
            holder.situation.setText("Alerta!");
            holder.status.setText(object.getLocal() + " está alagada, região possivelmente intransitável!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
        }

        if (object.getStatus() == 3 && object.getType() == 1) {
            holder.situation.setText("Alerta!");
            holder.status.setText(object.getLocal() + " transbordou, região possivelmente intransitável!");
            holder.cardView.setCardBackgroundColor(Color.parseColor("#F7DD2C00"));
        }


    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }
}
