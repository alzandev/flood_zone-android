package com.alzan.floodzone.Statistic.Latest;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.alzan.floodzone.R;

/**
 * Created by Calazans on 12/09/2018.
 */

public class RecyclerHolderAll extends RecyclerView.ViewHolder {

    TextView status;
    TextView situation;
    TextView timeStamp;
    CardView cardView;


    public RecyclerHolderAll(View itemView) {
        super(itemView);

        status = (TextView) itemView.findViewById(R.id.textStatus);
        cardView = (CardView) itemView.findViewById(R.id.cardStatus);
        timeStamp = (TextView) itemView.findViewById(R.id.timeStamp);
        situation = (TextView) itemView.findViewById(R.id.situation);

    }
}
