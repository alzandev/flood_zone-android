package com.alzan.floodzone.Statistic;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alzan.floodzone.Map.MainActivity;
import com.alzan.floodzone.Map.MapsFragment;
import com.alzan.floodzone.R;
import com.alzan.floodzone.Statistic.Latest.ListItems;
import com.alzan.floodzone.Statistic.Latest.RecyclerAdapterAll;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Calazans on 15/07/2018.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerHolder> {
    Context context;
    public Activity activity;
    private ArrayList<StatisticItem> objectList;
    StatisticItem object;
    MapsFragment mapsFragment;
    MainActivity mainActivity;
    RecyclerAdapterAll recyclerAdapter = null;
    LinearLayoutManager linearLayoutManager;


    ArrayList<ListItems> items2 = new ArrayList<ListItems>();


    public RecyclerAdapter(Context context, int textViewResourceId, ArrayList objects) {
        this.context = context;
        objectList = objects;
    }

    @Override
    public RecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.statistic_list, null);
        RecyclerHolder holder = new RecyclerHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final RecyclerHolder holder, int position) {
        object = objectList.get(position);

        getJSON();

        holder.local.setText(object.getLocal());

        holder.progressBarGreen.setMax(object.getAlertTotal());
        holder.progressBarYellow.setMax(object.getAlertTotal());
        holder.progressBarRed.setMax(object.getAlertTotal());

        holder.progressBarGreen.setProgress(object.getAlertGreen());
        holder.progressBarYellow.setProgress(object.getAlertYellow());
        holder.progressBarRed.setProgress(object.getAlertRed());

        holder.countGreen.setText(object.getAlertGreen() + "/" + object.getAlertTotal());
        holder.countYellow.setText(object.getAlertYellow() + "/" + object.getAlertTotal());
        holder.countRed.setText(object.getAlertRed() + "/" + object.getAlertTotal());

        if (object.getAlertGreen() == 0) {
            holder.progressBarGreen.setVisibility(View.GONE);
            holder.countGreen.setVisibility(View.GONE);

        }

        if (object.getAlertYellow() == 0) {
            holder.progressBarYellow.setVisibility(View.GONE);
            holder.countYellow.setVisibility(View.GONE);

        }

        if (object.getAlertRed() == 0) {
            holder.progressBarRed.setVisibility(View.GONE);
            holder.countRed.setVisibility(View.GONE);
        }

        if (object.getAlertRed() == 0 && object.getAlertYellow() == 0 && object.getAlertGreen() == 0) {
            holder.null_log.setVisibility(View.VISIBLE);
        } else {
            holder.null_log.setVisibility(View.GONE);
        }

        Log.d("ID_IN_ADAPTER", String.valueOf(object.getId()));


        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recyclerInRecycler.setLayoutManager(linearLayoutManager);

        final int positionFinal = position;


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.card2.getVisibility() == View.VISIBLE) {
                    holder.card2.setVisibility(View.GONE);
                } else if (holder.card2.getVisibility() == View.GONE) {
                    holder.card2.setVisibility(View.VISIBLE);
                    getJSON(String.valueOf(objectList.get(positionFinal).getId()), holder);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return objectList.size();
    }

    public void getJSON() {
        String url = "https://route.api.here.com/routing/7.2/calculateroute.json?app_id=m6NJsmFT3Q3ZrjTucEyC&app_code=sx9dTE2tGuae5ro_WCFrsQ&waypoint0=geo!-23.550347,-46.634219&waypoint1=geo!-23.546750,-46.6457375&mode=fastest;car;traffic:disabled";

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            getDataJson(response);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RetryPolicy retryPolicy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        queue.add(stringRequest);
    }

    public void getDataJson(String response) throws JSONException {

        JSONObject jsonObject = new JSONObject(response);
        JSONObject responseJSON = jsonObject.getJSONObject("response");
        JSONArray jsonArray = responseJSON.getJSONArray("route");

        items2.clear();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject object = (JSONObject) jsonArray.get(i);
            JSONArray legJSON = object.getJSONArray("leg");
            for (int j = 0; j < legJSON.length(); j++) {
                JSONObject arrayInArray = (JSONObject) legJSON.get(j);
                JSONArray maneuver = arrayInArray.getJSONArray("maneuver");
                System.out.println(" JSON 0 ===>  " + maneuver);

                for (int k = 0; k < maneuver.length(); k++) {
                    JSONObject maneuverJSON = (JSONObject) maneuver.get(k);
                    JSONObject position = maneuverJSON.getJSONObject("position");

                    String latitude = position.getString("latitude");

                    System.out.println(" JSON ===>  "  + latitude);


                }
            }
        }
    }

    public void getJSON(String url, final RecyclerHolder holder) {

        Log.d("NAME JSON", "http://alzan.com.br/FloodZone/flood_chart.php?id=" + url);

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://alzan.com.br/FloodZone/flood_chart.php?id=" + url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            getDataJson(response, holder);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RetryPolicy retryPolicy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(retryPolicy);

        queue.add(stringRequest);
    }


    public void getDataJson(String response, RecyclerHolder holder) throws JSONException {

        JSONObject jsonObject = new JSONObject(response);
        JSONArray jsonArray = jsonObject.getJSONArray("flood");
        items2.clear();

        for (int i = 0; i < jsonArray.length(); i++) {
            final ListItems listItems = new ListItems();
            JSONObject object = (JSONObject) jsonArray.get(i);

            int id = object.isNull("ID") ? null : object.getInt("ID");
            listItems.setId(id);

            int status = object.isNull("FLOOD") ? null : object.getInt("FLOOD");
            listItems.setStatus(status);

            int type = object.isNull("TYPE") ? null : object.getInt("TYPE");
            listItems.setType(type);

            String timeStamp = object.isNull("TIMESTAMP") ? null : object.getString("TIMESTAMP");
            listItems.setTimeStamp(timeStamp);

            String local = object.isNull("LOCAL") ? null : object.getString("LOCAL");
            listItems.setLocal(local);

            Log.d("LOCAL_IN_ADAPTER", id + "    " + local + "   " + timeStamp);

            items2.add(listItems);
        }

        holder.recyclerInRecycler.setVisibility(View.VISIBLE);
        holder.progressBar.setVisibility(View.GONE);

        recyclerAdapter = new RecyclerAdapterAll(context, R.layout.status_list, items2);
        holder.recyclerInRecycler.setAdapter(recyclerAdapter);

    }
}