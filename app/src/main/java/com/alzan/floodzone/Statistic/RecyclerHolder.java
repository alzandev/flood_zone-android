package com.alzan.floodzone.Statistic;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alzan.floodzone.R;

/**
 * Created by Calazans on 15/07/2018.
 */

public class RecyclerHolder extends RecyclerView.ViewHolder {

    TextView local;
    ProgressBar progressBarGreen;
    ProgressBar progressBarYellow;
    ProgressBar progressBarRed;

    CardView card2;

    TextView countRed;
    TextView countYellow;
    TextView countGreen;
    TextView null_log;

    ProgressBar progressBar;

    RecyclerView recyclerInRecycler;

    CardView cardView;

    public RecyclerHolder(View itemView) {
        super(itemView);

        cardView = (CardView) itemView.findViewById(R.id.cardView);
        card2 = (CardView) itemView.findViewById(R.id.card2);

        local = (TextView) itemView.findViewById(R.id.local);
        progressBarRed = (ProgressBar) itemView.findViewById(R.id.progressBarRed);
        progressBarYellow = (ProgressBar) itemView.findViewById(R.id.progressBarYellow);
        progressBarGreen = (ProgressBar) itemView.findViewById(R.id.progressBarGreen);

        progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);

        countRed = (TextView) itemView.findViewById(R.id.countRed);
        countYellow = (TextView) itemView.findViewById(R.id.countYellow);
        countGreen = (TextView) itemView.findViewById(R.id.countGreen);

        null_log = (TextView) itemView.findViewById(R.id.null_log);

        recyclerInRecycler = (RecyclerView) itemView.findViewById(R.id.recyclerInRecycler);


    }
}
