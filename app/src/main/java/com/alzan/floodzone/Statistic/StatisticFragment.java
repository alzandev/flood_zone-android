package com.alzan.floodzone.Statistic;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.alzan.floodzone.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

/**
 * Created by Calazans on 17/02/2018.
 */

public class StatisticFragment extends Fragment {

    RecyclerView recyclerView;
    public static RecyclerAdapter recyclerAdapter = null;
    static ArrayList<StatisticItem> list = new ArrayList<StatisticItem>();
    ProgressBar progressBar;


    public static StatisticFragment newInstance() {
        StatisticFragment fragment = new StatisticFragment();
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_statistic, container, false);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        getActivity().setTitle("Registros");


        new JSONTask(getContext(), recyclerView, recyclerAdapter, progressBar).execute();

        return view;
    }
}


class JSONTask extends AsyncTask<String, Integer, Integer> {

    public Context activity;
    ArrayList<StatisticItem> list = new ArrayList<StatisticItem>();
    public RecyclerView recyclerView;
    public RecyclerAdapter recyclerAdapter;
    public ProgressBar progressBar;

    public JSONTask(Context activity, RecyclerView recyclerView, RecyclerAdapter recyclerAdapter, ProgressBar progressBar) {
        this.activity = activity;
        this.recyclerView = recyclerView;
        this.recyclerAdapter = recyclerAdapter;
        this.progressBar = progressBar;
    }

    @Override
    protected Integer doInBackground(String... strings) {

//        try {
//            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
//            HttpGet httpGet = new HttpGet("http://alzan.com.br/FloodZone/flood.php");
//            HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
//            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
//            String json = reader.readLine();
//
//            JSONObject response = new JSONObject(json);
//            JSONArray jsonArray = response.getJSONArray("flood");
//
//            list.clear();
//
//            for (int i = 0; i < jsonArray.length(); i++) {
//                StatisticItem statusItem = new StatisticItem();
//                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
//
//                String local = jsonObject.isNull("LOCAL") ? null : jsonObject.getString("LOCAL");
//                statusItem.setLocal(local);
//
//                list.add(statusItem);
//            }
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }

        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet("http://alzan.com.br/FloodZone/flood.php?order=LOCAL");
            HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
            String json = reader.readLine();

            JSONObject response = new JSONObject(json);
            JSONArray jsonArray = response.getJSONArray("flood");
            list.clear();

            for (int i = 0; i < jsonArray.length(); i++) {
                StatisticItem statusItem = new StatisticItem();

                JSONObject jsonObject = (JSONObject) jsonArray.get(i);

                int ID = jsonObject.isNull("ID") ? null : jsonObject.getInt("ID");
                statusItem.setId(ID);

                String local = jsonObject.isNull("LOCAL") ? null : jsonObject.getString("LOCAL");
                statusItem.setLocal(local);

                try {
                    DefaultHttpClient defaultHttpClientChart = new DefaultHttpClient();
                    HttpGet httpGetChart = new HttpGet("http://alzan.com.br/FloodZone/flood_chart.php?id=" + ID);
                    HttpResponse httpResponseChart = defaultHttpClientChart.execute(httpGetChart);
                    BufferedReader readerChart = new BufferedReader(new InputStreamReader(httpResponseChart.getEntity().getContent(), "UTF-8"));
                    String jsonChart = readerChart.readLine();

                    JSONObject responseChart = new JSONObject(jsonChart);
                    JSONArray jsonArrayChart = responseChart.getJSONArray("flood");

                    int redAlert = 0, yellowAlert = 0, greenAlert = 0, total = 0;

                    for (int j = 0; j < jsonArrayChart.length(); j++) {
                        JSONObject jsonObjectChart = (JSONObject) jsonArrayChart.get(j);

                        int floodChart = jsonObjectChart.isNull("FLOOD") ? null : jsonObjectChart.getInt("FLOOD");
                        String localChart = jsonObjectChart.isNull("LOCAL") ? null : jsonObjectChart.getString("LOCAL");
                        String timestamp = jsonObjectChart.isNull("TIMESTAMP") ? null : jsonObjectChart.getString("TIMESTAMP");

                        if (floodChart == 1) {
                            greenAlert++;
                        } else if (floodChart == 2) {
                            yellowAlert++;
                        } else if (floodChart == 3) {
                            redAlert++;
                        }

                        Log.d("ALERT_LEVELS", localChart + " " + timestamp);

                        total++;
                    }

                    statusItem.setAlertGreen(greenAlert);
                    statusItem.setAlertYellow(yellowAlert);
                    statusItem.setAlertRed(redAlert);
                    statusItem.setAlertTotal(total);

                    Log.d("ALERT_LEVELS", greenAlert + " " + yellowAlert + " " + redAlert + " " + total);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                list.add(statusItem);
            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(Integer numero) {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

        recyclerAdapter = new RecyclerAdapter(activity, R.layout.statistic_list, list);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerAdapter);

    }
}