package com.alzan.floodzone.Statistic;

/**
 * Created by Calazans on 15/07/2018.
 */

public class StatisticItem {
    private int id;
    private int status;
    private String local;
    private String timeStamp;

    private int alertRed;
    private int alertYellow;
    private int alertGreen;
    private int alertTotal;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getAlertRed() {
        return alertRed;
    }

    public void setAlertRed(int alertRed) {
        this.alertRed = alertRed;
    }

    public int getAlertYellow() {
        return alertYellow;
    }

    public void setAlertYellow(int alertYellow) {
        this.alertYellow = alertYellow;
    }

    public int getAlertGreen() {
        return alertGreen;
    }

    public void setAlertGreen(int alertGreen) {
        this.alertGreen = alertGreen;
    }

    public int getAlertTotal() {
        return alertTotal;
    }

    public void setAlertTotal(int alertTotal) {
        this.alertTotal = alertTotal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public StatisticItem() {
    }

    public StatisticItem(int status, String local) {
        this.status = status;
        this.local = local;
    }

    public StatisticItem(int id, int status, String local) {
        this.id = id;
        this.status = status;
        this.local = local;
    }

    public StatisticItem(int id, int status, String local, String timeStamp) {
        this.id = id;
        this.status = status;
        this.local = local;
        this.timeStamp = timeStamp;
    }

}
